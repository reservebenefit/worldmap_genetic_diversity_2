library("bold")

taxonRequest <- "Actinopterygii"

resBold <- bold_seqspec(taxon=taxonRequest, sepfasta=TRUE)

## Saving object in RData format
resboldObjFile=paste("rdata/bold_res_",taxonRequest,".RData",sep="")
save(resBold, file = resboldObjFile)

