Reef-futures social variables metadata :


#------------------------------------------ AT COUNTRY SCALE

#---- NGO
Variable: 
Number of NGO in the country

Dataset name: 

Web address: 

Detailed reference: 
﻿
Accessed via:

Type:
﻿Raw number of NGO

Initial spatial resolution:
Country

Time range:

Time resolution:

Unit:
None

Transformations:
None

Processing steps:
• load 
• for each SurveyID associate the number of NGO using the country
_________________________________________________________________________

#---- NoViolence    
Variable: 
Index of violence in the country

Dataset name: 

Web address: 

Detailed reference: 
﻿
Accessed via:

Type:
﻿ Index from -1.396154 (Egypt) to 1.905166 (Greenland). 

Initial spatial resolution:
Country

Time range:

Time resolution:

Unit:
None

Transformations:
None

Processing steps:
• load 
• for each SurveyID associate the value of index using the country
_________________________________________________________________________

#---- Voice
Variable: 
Freedom of speech index

Dataset name: 

Web address: 

Detailed reference: 
﻿
Accessed via:

Type:
﻿Index from -1.598 (China) to 1.683 (Norway). 

Initial spatial resolution:
Country

Time range:

Time resolution:

Unit:
None

Transformations:
None

Processing steps:
* load 
* for each SurveyID associate the value of index using the country
_________________________________________________________________________


#---- ControlofCorruption 
Variable: 
Number of NGO in the country

Dataset name: 

Web address: 


Detailed reference: 
﻿
Accessed via:

Type:
﻿ Index from -1.194436 (Cambodia) to 2.280244 (New-zealand).

Initial spatial resolution:
Country

Time range:

Time resolution:

Unit:
None

Transformations:
None

Processing steps:
* load 
* for each SurveyID associate the value of index using the country
_________________________________________________________________________

#---- Naturalresourcesrents
Variable: 
Index of natural ressources (mine, oil, …)

Dataset name: 

Web address: 

Detailed reference: 
﻿
Accessed via:

Type:
﻿ Index from 0 (several islands like cook islands) to 44.57186 (East Timor).

Initial spatial resolution:
Country

Time range:

Time resolution:

Unit:
None

Transformations:
None

Processing steps:
* load 
* for each SurveyID associate the value of index using the country

_________________________________________________________________________

#---- Fisheries dependency
Variable: 
Index of dependency to fisheries

Dataset name: 

Web address: 

Detailed reference: 
﻿
Accessed via:

Type:
﻿ Index from 0.03852018 (South Africa) to 0.7221809 (Greenland).

Initial spatial resolution:
Country

Time range:

Time resolution:

Unit:
None

Transformations:
None

Processing steps:
* load 
* for each SurveyID associate the value of index using the country
_________________________________________________________________________

#---- HDI
Variable: 
Index of dependency to fisheries

Dataset name: 
Human Development Indicators and Indices

Web address: 
http://hdr.undp.org/en/2018-update

Detailed reference: 
2018 Statistical Update published by the United Nations of Development program (UNDP).

Accessed via:


Type:
﻿ Index from 0.03852018 (South Africa) to 0.7221809 (Greenland).


Initial spatial resolution:
Country

Time range: 2017

Time resolution: Year

Unit:
None

Transformations:
None

Processing steps:
* load 
* for each SurveyID associate the value of index using the country
_________________________________________________________________________

#---- Confllict
Variable: 
A conflict-year dataset with information on armed conflict where at least one party is the government of a state

Dataset name: UCDP/PRIO Armed Conflict Dataset version 19.1 

Web address: https://ucdp.uu.se/downloads/

Detailed reference: 
﻿
Accessed via:

Type:
﻿ Index from 0.03852018 (South Africa) to 0.7221809 (Greenland).

Initial spatial resolution:
Country

Time range: 1946-2018

Time resolution: Year

Unit:
None

Transformations:
None

Processing steps:
* We calculate a conflict score for each country which correspond to the sum of year of conflict in which the country is engaged 
* assigned the conflict score of country for each surveys.

_________________________________________________________________________

#---- Fish_landings 

Variable:  Annual average fish landings (reported and unreported catches) for the fish families reported in the RLS dataset (91 fish families out of 168 are included in the SAU data) over the period 2010-2014. 

Dataset name: Sea Around Us

Web address: http://www.seaaroundus.org 

Detailed reference: 
﻿
Accessed via:

Type: Catch data 

Initial spatial resolution:
Country

Time range: 2010-2014

Time resolution: Year

Unit:
tons 

Transformations:
None

Processing steps:
* We extracted fish landings (reported and unreported catches, discards were removed) of the fish families recorded in the RLS dataset (91 families out of 168). Annual average (in tons) over the period 2010-2014 was computed.
* assigned the fish landings value of the country to each survey.

#--------------------------------------------------------------------------------- AT SURVEY SCALE
#---- Travel time to the nearest human
Variable: travel time in minutes from 

Dataset name: 

Web address: 


Detailed reference: 
﻿Maire, Eva, et al. "How accessible are coral reefs to people? A global assessment based on travel time." Ecology letters 19.4 (2016): 351-360.

Accessed via:

Type:
﻿
Initial spatial resolution:

Time range:

Time resolution:

Unit:

Transformations:

Processing steps:
* load 
* for each SurveyID associate the value of index using the country
_________________________________________________________________________

#---- Total gravity
Variable: 

Dataset name: 

Web address: 

Detailed reference: 
﻿Maire, Eva, et al. "How accessible are coral reefs to people? A global assessment based on travel time." Ecology letters 19.4 (2016): 351-360.

Accessed via:

Type:
﻿
Initial spatial resolution:

Time range:

Time resolution:

Unit:

Transformations:

Processing steps:

____________________________________________________________________