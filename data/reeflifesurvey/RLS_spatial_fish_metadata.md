Document to track and changes to the raw Reef Life Survey data through time and keep track of any divergences from raw data to modified data: 

RLS spatial fish data extract.csv (raw RLS extract)
* Uploaded by Rick-Stuart Smith on 8th August 2019. 

RLS-spatial-fish-data-extract-siteSummary.csv
* 7th November 2019: Created from the script 01-converting-species-to-site-data.R. To produce survey level aggregate numerical abundance and biomass. 
* 21st November 2019: ISO-3166-1 ALPHA-3 added 
* 21st November 2019: update 70 out of data names in the TAXONOMIC_NAME field

RLS-spatial-fish-data-extract-surveyXspecies.csv
* 11th November 2019: Create from the script 01-converting-species-to-site-data.R. Provides species by site level numerical abundance and biomass values. 
* 21st November 2019: ISO-3166-1 ALPHA-3 added 
* 21st November 2019: update 70 out of data names in the TAXONOMIC_NAME field