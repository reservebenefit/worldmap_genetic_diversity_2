# worldmap_genetic_diversity_2

Here we go again to investigate genetic patterns at different scale


# Installation

## Prerequisites

* python3
* bash
* lynx
* awk



### CONDA

Instructions to install CONDA are [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)


### JUPYTER NOTEBOOK

* install jupyter notebook using CONDA

```
conda install -c conda-forge notebook
```

### R 

```
conda env create -f conda/env_ubuntu.yml
conda activate mapmarine2

```

If you haven’t done this already, you will have to make Jupyter see the newly installed R kernel by installing a kernel spec. The kernel spec can be installed for the current user with the following line from R: 

```
IRkernel::installspec()
```

### geogendiv


Into R session:
```
devtools::install_github("grelot/geogendivr", force = TRUE)
```




# 1 Donwload and format BOLD database

1. I seek the term 'actinopterygii' on [BOLD website](http://www.boldsystems.org).
2. I download Combined TSV files on **monday the 6th january 2020**.
3. The original file is stored into [01-bold_raw](01-bold_raw) folder.


# 2 Filter BOLD database

1. Keep sequences with `latitude/longitude` `sequence`, `species` and `markercode` information.
2. Add `latitude/longitude` information using GEONAMES from `region` information.
3. Filter out sequences which belong to **COI**.
4. File with COI georeferenced sequences is stored as [02-bold_format/all_format_bold_coi.tsv](02-bold_format/all_format_bold_coi.tsv)

```
bash 00-scripts/step2/format_bold.sh
```

# 3 

