## Download raw BOLD database

1. I type the word 'actinopterygii' on [BOLD website](http://www.boldsystems.org/index.php/Public_SearchTerms).

2. I download Combined TSV files on **monday the 6th january 2020**.

3. The original file is stored into [01-bold_raw](01-bold_raw) folder.
