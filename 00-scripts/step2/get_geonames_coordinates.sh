##########################################################################
## Codes for the paper:
## ...
## Authors :
## Stephanie Manel, Pierre-Edouard Guerin, ...
## 
## Montpellier, 2020-...
## ...
##
##########################################################################
##
## Uses [http://www.geonames.org/] to find 
## missing coordinates of individual sequences 
## from their textual information of location.
##
##########################################################################

## linux-formated bold table
FORMAT_BOLD="01-bold_raw/bold_data.tsv"

## get samples with no lat/lon information but region name
REG_BOLD="02-bold_format/bold_data_region.tsv"
awk -F "\t" '{if ( ($47=="" || $48=="") && ($59!="" ))  print $0}' $FORMAT_BOLD | awk -F "\t" '{ if($22 != "") print $0 }' | awk -F "\t" '{ test= match( $72,"[RWSNYMKHBVD_]"); if( test ==0) print $0}' > $REG_BOLD


## get GPS coordinates using geonames from region information
GEONAMES_ASSIGNED_REG_BOLD=${REG_BOLD/.tsv/_geonames_assigned.tsv}
GEONAMES_ONLY="02-bold_format/latlon_geonames.tsv"
MERGED_GEONAMES="02-bold_format/latlon_geonames_merged.tsv"
LEFT_LATLON="02-bold_format/left_latlon.tsv"
RIGHT_LATLON="02-bold_format/right_latlon.tsv"

cut -f 1-46 $REG_BOLD > $LEFT_LATLON
cut -f 49- $REG_BOLD > $RIGHT_LATLON

cat $REG_BOLD | while read ligne;
do

recherche=`echo -e "$ligne" | awk -F "\t" '{print $57"_"$58"_"$59}' | sed 's/_/+/g' | sed -e 's/[\,.:;$~)(=*%?!-]/+/g' | sed -e 's/\([^[:blank:]]\)\([[:upper:]]\)/\1+\2/g' | sed -e 's/+$//g'  | sed -e 's/ /+/g'`
requete="http://www.geonames.org/search.html?q="$recherche
#echo $requete
result=`lynx -dump $requete | grep "Lat/Lng" | head -1 | awk '{ print $3"\t"$5}' | sed 's/\[10\]//g'`
#echo $result
if [[ -z $result ]]
then
  #echo "zut"
  lat=`lynx -dump $requetelynx -dump $requete | egrep -o "[SN].[0-9]+°.[0-9]+'.[0-9]+''" | head -1 `
  #echo $lat
  lng=`lynx -dump $requetelynx -dump $requete | egrep -o "[WE].[0-9]+°.[0-9]+'.[0-9]+''" | head -1 `
  #echo $lng
  if [[ ( -z $lat ) || ( -z $lng ) ]]
  then
    printf "no_lat_and_no_lon\n"
  else
    result=`python2 00-scripts/step1/lat_long_DMS_DD_converter.py -dms "$lat $lng"`
    echo -e "$result"
  fi
else
  echo -e "$result"
fi
done > $GEONAMES_ONLY

paste $LEFT_LATLON $GEONAMES_ONLY $RIGHT_LATLON > $MERGED_GEONAMES
grep -a -v "no_lat_and_no_lon" $MERGED_GEONAMES > $GEONAMES_ASSIGNED_REG_BOLD
