##########################################################################
## Codes for the paper:
## ...
## Authors :
## Stephanie Manel, Pierre-Edouard Guerin, ...
## 
## Montpellier, 2020-...
## ...
##
##########################################################################
##
## Keep only the COI-5P sequences with lat/lon information
##
##########################################################################

## raw BOLD database file relative path
RAW_BOLD="01-bold_raw/bold_data.txt"
## format LINUX file
FORMAT_BOLD=${RAW_BOLD/.txt/.tsv}
sed -e "s/\r//g" $RAW_BOLD > $FORMAT_BOLD

#remove samples with no lat/lon information
NO_LATLON_BOLD="02-bold_format/bold_nolatlon.tsv"
awk -F "\t" '{ if( $47 != "" && $48 != "") print $0 }' $FORMAT_BOLD > $NO_LATLON_BOLD
#remove samples with no species name
NO_SPEC_BOLD=${NO_LATLON_BOLD/.tsv/_nospec.tsv}
awk -F "\t" '{ if($22 != "") print $0 }' $NO_LATLON_BOLD > $NO_SPEC_BOLD
#remove samples with no sequences or sequences with IUAPC ambiguity i.e. polybase characters (e.g. "N")
NO_SEQ_BOLD=${NO_SPEC_BOLD/.tsv/_noseq.tsv}
awk -F "\t" '{ test= match( $72,"[RWSNYMKHBVD_]"); if( test ==0) print $0}' $NO_SPEC_BOLD > $NO_SEQ_BOLD

## get information lat/lon with geonames for data with missing lat/lon information but region
bash 00-scripts/step1/get_geonames_coordinates.sh
GEONAMES_ASSIGNED_REG_BOLD="02-bold_format/bold_data_region_geonames_assigned.tsv"

ALL_FORMAT_BOLD="02-bold_format/all_format_bold.tsv"
cat $NO_SEQ_BOLD $GEONAMES_ASSIGNED_REG_BOLD > $ALL_FORMAT_BOLD

## keep locus name aka markercode named "COI-5P"
COI_BOLD=${ALL_FORMAT_BOLD/.tsv/_coi.tsv}
awk -F "\t" '{ if($70 == "COI-5P" || $70 == "markercode") print $0 }' $ALL_FORMAT_BOLD > $COI_BOLD

## keep sequence with a length greater than 420 bases
COI_LENGHT_BOLD=${COI_BOLD/.tsv/_length420.tsv}
awk -F "\t" 'NR == 1 {print; next} { if(length($72) > 420) print $0 }' $COI_BOLD > $COI_LENGHT_BOLD

## keep species with at least 2 sequences
SPECIES_WITH_A_SINGLE_SEQUENCE="02-bold_format/species_with_a_single_sequence.txt"
COI_2SEQ_SPECIES=${COI_LENGHT_BOLD/.tsv/_atleast2seqbyspecies.tsv}
sort -k22 -t$'\t' $COI_LENGHT_BOLD | awk -F "\t" '{ print $22 }' | uniq -c | awk '{ if($1 < 2) print $2" "$3 }' > $SPECIES_WITH_A_SINGLE_SEQUENCE


grep -wvf $SPECIES_WITH_A_SINGLE_SEQUENCE $COI_LENGHT_BOLD > $COI_2SEQ_SPECIES


sort -k22 -t$'\t' $COI_LENGHT_BOLD | awk -F "\t" '{ print $22 }' | uniq -c | awk '{ if($1 < 2) print $2" "$3" "$4" "$5 }'


sed 's/ /[[:space:]]/g' $SPECIES_WITH_A_SINGLE_SEQUENCE